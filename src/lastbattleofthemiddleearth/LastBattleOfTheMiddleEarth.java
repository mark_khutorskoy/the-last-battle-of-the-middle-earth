/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lastbattleofthemiddleearth;

//import java.util.ArrayList;
import java.util.*;
import java.io.*;
import middleearth.*;
import static middleearth.MiddleEarthCitizen.getCountryOrigin;
import java.util.Collections;
//import java.io.Serializable;
//import java.util.Random;
/**
 *
 * @author MarkNotebook
 */
public class LastBattleOfTheMiddleEarth {
    
     public static ArrayList<MiddleEarthCitizen> fillArmy(ArrayList<String> armyTypes)
    {
        
        ArrayList <MiddleEarthCitizen> army = new ArrayList<MiddleEarthCitizen>();
        for (int i=0; i<armyTypes.size(); i++)
        {
            int max_quant = 10;
            Random rndQuantity = new Random();
            int quantity = 1+ rndQuantity.nextInt(max_quant); //количество юнитов одного типа          
                  
            switch(armyTypes.get(i).toString()){
                case "Human": 
                    System.out.println("Люди: "+ quantity);
                    army = MergeLists(army, fillHumans(quantity));
                    break;                   
                case "Rohhirim": 
                    System.out.println("Роххиримы: "+ quantity);
                    army = MergeLists(army, fillRohhirims(quantity));
                    break;     
                case "Wizard":
                    quantity = 0 + rndQuantity.nextInt(1-0+1);
                    System.out.println("Волшебник: "+ quantity);                  
                    if(quantity!=0)army = MergeLists(army, fillWizard(quantity));
                    break;     
                case "WoodenElf":
                    System.out.println("Лесные Эльфы: "+ quantity);
                    army = MergeLists(army, fillWoodenElves(quantity));
                    break;    
                case "Elf":
                    System.out.println("Эльфы: "+ quantity);
                    army = MergeLists(army, fillElves(quantity));
                    break;               
                case "Goblin":
                    System.out.println("Гоблины: "+ quantity);
                    army = MergeLists(army, fillGoblins(quantity));
                    break;    
                case "Troll": 
                    System.out.println("Тролли: "+ quantity);
                    army = MergeLists(army, fillTrolls(quantity));
                    break;          
                case "Orc":
                    System.out.println("Орки: "+ quantity);
                    army = MergeLists(army, fillOrcs(quantity));
                    break;
                case "UrukHai":
                    System.out.println("Урук-хаи: "+ quantity);
                    army = MergeLists(army, fillUruks(quantity));
                    break;
            }
            //System.out.println("Количество: "+ quantity);
        }
        return army;
    }

    public static ArrayList<Human> fillHumans(int humanQuantity)
    {
        ArrayList<Human> humans = new ArrayList<Human>();
        String homeTowns[] = {"Гондор", "Рохан"}; //выделение с инициализацией
        //String humanNames[]= {"Эомер", "Фарамир", "Боромир", "Беретор", "Эленгост", "Теоден", "Эовин", "Теодред", "Гамлик","Гримбольд","Арагорн","Арадорн","Эктелион"};
        String humanNames[]={"Человек"};
        for (int i=0;i<humanQuantity;i++)
        {
            Random rnd = new Random();
            int curTown = rnd.nextInt(homeTowns.length);
            int curName = rnd.nextInt(humanNames.length);
            humans.add(new Human(homeTowns[curTown] ,18 + rnd.nextInt(45 - 18 + 1),160 + rnd.nextInt(210 - 160 + 1),humanNames[curName],7 + rnd.nextInt(8 - 7 + 1), false));
        }
        return humans;
    }
    
    public static ArrayList<Rohhirim> fillRohhirims(int rohhirimQuantity)
    {
        ArrayList<Rohhirim> rohhirims = new ArrayList<Rohhirim>();
        //String rohhirimNames[]= {"Эомер","Теоден", "Эовин", "Теодред", "Гамлик","Гримбольд"};
        String rohhirimNames[]= {"Роххирим"};
        for (int i=0;i<rohhirimQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(rohhirimNames.length);
            int curHorsePower = 4 + rnd.nextInt(5 - 4 + 1);
            int curHumanPower = 7 + rnd.nextInt(8 - 7 + 1);
            rohhirims.add(new Rohhirim(rohhirimNames[curName],18 + rnd.nextInt(45 - 18 + 1),(curHorsePower+curHumanPower),true));
        }
        return rohhirims;
    }
    
    public static ArrayList<Wizard> fillWizard(int wizardQuantity)
    {
        ArrayList<Wizard> wizard = new ArrayList<Wizard>();
        String wizardNames[]= {"Гендальф"};
        for (int i=0;i<wizardQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(wizardNames.length);
            int curHorsePower = 4 + rnd.nextInt(5 - 4 + 1);
            wizard.add(new Wizard(wizardNames[curName],210,(curHorsePower+20), true));
        }
        return wizard;
    }
    
    public static ArrayList <Troll>  fillTrolls(int trollQuantity){
     ArrayList<Troll> trolls = new ArrayList<Troll>();
        //String trollNames[] = {"Том","Берт","Билл"};
        String trollNames[] = {"Троль"};
        String trollTypes[] = {"Пещерный","Каменный", "Горный","Холмовой","Снежный","Олог-хай"};
        for (int i=0;i<trollQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(trollNames.length);
            int curType = rnd.nextInt(trollTypes.length);
            trolls.add(new Troll(trollTypes[curType],360 + rnd.nextInt(490 - 360 + 1),trollNames[curName],11 + rnd.nextInt(15 - 11 + 1),false));
            //System.out.println(trolls[i].toString());
        }
        return trolls;
    }
    
    public static ArrayList <Goblin>  fillGoblins(int goblinQuantity)
    {
        ArrayList<Goblin> goblins = new ArrayList<Goblin>();
        String goblinNames[] = {"Гоблин"};
        for (int i=0;i<goblinQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(goblinNames.length);
            goblins.add(new Goblin(goblinNames[curName],160,2 + rnd.nextInt(5 - 2 + 1), false));
        }
        return goblins; 
    }
    
    public static ArrayList <? extends MiddleEarthCitizen>  fillOrcs(int orcQuantity)
    {
        ArrayList<Orc> orcs = new ArrayList<Orc>();
        //ArrayList<Wolf> wolves = new ArrayList<Wolf>();
        //ArrayList<MiddleEarthCitizen> orcsAndWolves = new ArrayList<MiddleEarthCitizen>();
        //String orcNames[] = {"Азог Завоеватель", "Лурц", "Готмог", "Шарку", "Болдог", "Больг Жестокий", "Гольфимбул", "Горбаг", "Углук"};
        String orcNames[] = {"Орк"};
        for (int i=0;i<orcQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(orcNames.length);
            int power = (8 + rnd.nextInt(10 - 8 + 1))+(4 + rnd.nextInt(7 - 4 + 1));
            orcs.add(new Orc(orcNames[curName],170,power, true));
            //wolves.add(new Wolf("Варг",70,4 + rnd.nextInt(7 - 4 + 1),false,i));
        }
        //orcsAndWolves = MergeLists(orcs, wolves);
        //return orcsAndWolves; 
        return orcs;
    }
    
    public static ArrayList <UrukHai>  fillUruks(int urukQuantity)
    {
        ArrayList<UrukHai> uruks = new ArrayList<UrukHai>();
        String urukNames[] = {"Урук-хай"};
        for (int i=0;i<urukQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(urukNames.length);
            uruks.add(new UrukHai(urukNames[curName],185,10 + rnd.nextInt(12 - 10 + 1), false));
        }
        return uruks; 
    }
    
//    public static ArrayList <Wolf>  fillWolves(int wolfQuantity)
//    {
//        ArrayList<Wolf> wolves = new ArrayList<Wolf>();
//        String wolvesNames[] = {"Варг"};
//        for (int i=0;i<wolfQuantity;i++)
//        {
//            Random rnd = new Random();
//            int curName = rnd.nextInt(wolvesNames.length);
//            wolves.add(new Wolf(wolvesNames[curName],70,4 + rnd.nextInt(7 - 4 + 1), false));
//        }
//        return wolves; 
//    }
    
    public static ArrayList <WoodenElf>  fillWoodenElves(int woodenElvesQuantity)
    {
        ArrayList<WoodenElf> woodenElves = new ArrayList<WoodenElf>();
        String woodenElvesNames[] = {"Лесной эльф"};
        for (int i=0;i<woodenElvesQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(woodenElvesNames.length);
            woodenElves.add(new WoodenElf(woodenElvesNames[curName],200,6, false));
        }
        return woodenElves;   
    }
    
    public static ArrayList <Elf>  fillElves(int elvesQuantity)
    {
        ArrayList<Elf> elves = new ArrayList<Elf>();
        String elvesNames[] = {"Эльф"};
        for (int i=0;i<elvesQuantity;i++)
        {
            Random rnd = new Random();
            int curName = rnd.nextInt(elvesNames.length);
            elves.add(new Elf(elvesNames[curName],200,4 + rnd.nextInt(7 - 4 + 1), false));
        }
        return elves;   
    }
//    public static ArrayList <? extends MiddleEarthCitizen> fillClass(int quantity, String className)
//    {
//        switch (className){
//            case "Wolf": 
//                    army = MergeLists(army, fillHumans(quantity));
//                    break; 
//                    
//                case "UrukHai: 
//                    army = MergeLists(army, fillRohhirims(quantity));
//                    break; 
//                    
//                case "Orc": 
//                    army = MergeLists(army, fillTrolls(quantity));
//                    break; 
//                case "Goblin":
//                    army = MergeLists(army, fillWizard(quantity));
//                    break; 
//        }
//       
//        return a;
//    }
    
    public static ArrayList<MiddleEarthCitizen> MergeLists(ArrayList<? extends MiddleEarthCitizen> a, ArrayList<? extends MiddleEarthCitizen> b) 
    {
        ArrayList <MiddleEarthCitizen> army = new ArrayList<MiddleEarthCitizen>(a);
        army.addAll(b);
        
        return army;
    }
    
   
    public static void FirstPrint()
    {
        MiddleEarthCitizen citizen0 = new MiddleEarthCitizen();
        MiddleEarthCitizen citizen = new MiddleEarthCitizen("John",178,7,false);
        Human human = new Human("Gondor",28, 178, "John",7,false);
        // citizen.setName(); citizen.setHeight(178);
        
       // MiddleEarthCitizen.getCountryOrigin();
        getCountryOrigin();
        
       
        System.out.println(citizen.toString());
        System.out.println(human.toString());
        System.out.println(citizen.heightToMeters());
    }
    
    public static void SecondPrint()
    {
        final int humanQuantity = 80; final int trollQuantity = 20;
        
        ArrayList <Human> humans = fillHumans(humanQuantity);
        ArrayList <Troll> trolls = fillTrolls(trollQuantity);
        
        ArrayList <MiddleEarthCitizen> army = MergeLists(humans,trolls);
        for (int i=0;i<army.size();i++)
            System.out.println(army.get(i).toString());    
    }
    
    public static void ThirdPrint()
    {      
        ArrayList<String> armyTypes = new ArrayList<String>(Arrays.asList("Human","Rohhirim","Troll")); //на данный момент есть только люди, роххиримы и тролли
        ArrayList <MiddleEarthCitizen> army = fillArmy(armyTypes);

        PrintList(army);
        
        System.out.println("Все пешие в каждой армии ");
        CheckCriteria(army);
        
        System.out.println("Всадники с силой 12+ ");
        PrintWarriorsByPower(army);
        
        System.out.println("Среднее значение силы всех пеших юнитов в армии:");
        PrintWarriorsAvgPower(army);
        
        System.out.println("Сумма силы всех конных:");
        PrintWarriorsSumPower(army);
        
    }
    public static void PrintWarriorsAvgPower(ArrayList<MiddleEarthCitizen> list)
    {
        System.out.println(list.stream()
                .filter((i)->!(i instanceof Rohhirim || i instanceof Wizard || i instanceof Orc))
                .mapToDouble(MiddleEarthCitizen :: getPower).average().getAsDouble()
        );      
    }
    
    public static void PrintWarriorsSumPower(ArrayList<MiddleEarthCitizen> list)
    {
         System.out.println(list.stream()
                .filter((i)->(i instanceof Rohhirim || i instanceof Wizard || i instanceof Orc))
                .mapToInt(MiddleEarthCitizen :: getPower).sum());
    }
    public static void printWarriorsByCriteria(ArrayList<MiddleEarthCitizen> list, MESSearchCriteria x)
    {
        list.stream()
                .filter((i) -> (x.AcceptCriteria(i)))
                .forEachOrdered((i) -> {System.out.println(i.toString());});
        
        /*for (MiddleEarthCitizen i: list){
            if (x.AcceptCriteria(i))
            {
                System.out.println(i.toString());
            }
        }*/
    }
    //просто агрегатная функция
    public static void PrintWarriorsByPower(ArrayList<MiddleEarthCitizen> list)
    {
        list.stream()
                .filter((i)->i.getPower()>12 && (i instanceof Rohhirim || i instanceof Wizard || i instanceof Orc))
                .forEachOrdered((i)-> {System.out.println(i.toString());});
       
    }
    //проверка на пешего
    public static void CheckCriteria(ArrayList<MiddleEarthCitizen> list)
    {
        MESSearchCriteria onFeet = (x) -> !((x instanceof Rohhirim) || (x instanceof Wizard) || (x instanceof Orc));
        printWarriorsByCriteria(list, onFeet);
    }
    /**
     * @param args the command line arguments
     */
   
    
    public static int WhoIsFirst(MiddleEarthCitizen a, MiddleEarthCitizen b)
    {
        int first = 0; //число соотв. номеру аргумента
        if((a.getFirstStrike()==true && b.getFirstStrike()==true)||(a.getFirstStrike()==false && b.getFirstStrike()==false))
        {
            Random rnd = new Random();
            first = 1+rnd.nextInt(2);
        }
        else if (a.getFirstStrike()==true && b.getFirstStrike()==false)
        {
            first = 1;
        }
        else if (a.getFirstStrike()==false && b.getFirstStrike()==true)
        {
            first = 2;
        }
        return first;
    }
    
    public static int Damage(MiddleEarthCitizen a, MiddleEarthCitizen b) //возвращает номер аргумента-победителя
    {
        int damage = a.getPower()-b.getPower();
        int winner = 1;
        System.out.println(a.getName()+" нанес " + a.getPower()+" повреждений " +b.getName()+ (damage<0 ? " и не убил" : " и убил;"));
        
        while (damage<0)//не хватило убить
        {
            b.setPower(Math.abs(damage));
            winner = 2;
            damage = b.getPower()-a.getPower();   
            System.out.println(b.getName()+" нанес " + b.getPower()+" повреждений "+a.getName()+ (damage<0 ? " и не убил" : " и убил;"));
            if (damage<0)
            {
                a.setPower(Math.abs(damage));
                damage = a.getPower()-b.getPower(); 
                System.out.println(a.getName()+" нанес " + a.getPower()+" повреждений "+b.getName()+ (damage<0 ? " и не убил" : " и убил;"));
               // b.setPower(Math.abs(damage));
                winner = 1;
            }
        }   
        return winner;
    }
    
    public static void Fight(ArrayList<MiddleEarthCitizen> a, ArrayList<MiddleEarthCitizen> b)
    {
        while(a.size()>0 && b.size()>0)
        {
            ArrayList<MiddleEarthCitizen>aToRemove = new ArrayList<MiddleEarthCitizen>();
            ArrayList<MiddleEarthCitizen>bToRemove = new ArrayList<MiddleEarthCitizen>();
            int min = a.size()<b.size() ? a.size() : b.size();
            
            for (int i=0; i<min ;i++) //по меньшему
            {                   
                int first = WhoIsFirst(a.get(i), b.get(i));
                if(first==1)
                {
                 int winner = Damage(a.get(i), b.get(i)); //нашли кто кого убил
                 if (winner==1)
                 {
                     bToRemove.add(b.get(i)); //убрали проигравшего
                 } 
                 else 
                 {
                     aToRemove.add(a.get(i));
                 }  
                }
                else
                {
                 int winner = Damage(b.get(i), a.get(i)); //нашли кто кого убил
                 if (winner==1)
                 {
                     aToRemove.add(a.get(i)); //убрали проигравшего
                 } 
                 else 
                 {
                     bToRemove.add(b.get(i));
                 }
                }
            }
            
              a.removeAll(aToRemove);
              b.removeAll(bToRemove);
        }
        System.out.println();System.out.println("ВЫЖИВШИЕ: ");
        if (!a.isEmpty()) a.stream().forEach((i)-> {System.out.println(i.toString());});
        else if (!b.isEmpty()) b.stream().forEach((i)-> {System.out.println(i.toString());});
    }
    
    public static void PrintList(ArrayList <? extends MiddleEarthCitizen> list)
    {
        for (int i=0;i<list.size();i++)
        System.out.println(list.get(i).toString());
    }
   
    public static void Result(ArrayList <MiddleEarthCitizen> light1, ArrayList <MiddleEarthCitizen> light2,
                              ArrayList <MiddleEarthCitizen> dark1, ArrayList <MiddleEarthCitizen> dark2,int round) throws IOException
    {
        if((!light1.isEmpty() && !dark2.isEmpty()) || (!light2.isEmpty() && !dark1.isEmpty()))
        {
            //return false;    
            System.out.println("3й РАУНД:");
            if (!light1.isEmpty())
            {
                Fight(light1,dark2);
                Result(light1,light2,dark1,dark2,3);  
            }
            else 
            {
                Fight(light2,dark1);
                Result(light1,light2,dark1,dark2,3);  
            }
        }
        else 
        {
            //return true;
            System.out.println(round+" раундов хватило.");
            if (!light1.isEmpty() || !light2.isEmpty()) 
            {
                System.out.println("ПОБЕДИЛ СВЕТ");
                Serialize(MergeLists(light1, light2));
            }
            else if (!dark1.isEmpty() || !dark2.isEmpty())
            {
                System.out.println("ПОБЕДИЛА ТЬМА");
                Serialize(MergeLists(dark1, dark2));
            }
            
        }        
    }
    
    public static void LastBattle() throws IOException
    {
        ArrayList<String> lightArmyTypes = new ArrayList<String>(Arrays.asList("Human","Rohhirim","Wizard","Elf","WoodenElf"));
        ArrayList<String> lightArmyRiderTypes = new ArrayList<String>(Arrays.asList("Rohhirim","Wizard"));
        ArrayList<String> lightArmyFeetTypes = new ArrayList<String>(Arrays.asList("Human","Elf","WoodenElf"));
        
        ArrayList<String> darkArmyTypes = new ArrayList<String>(Arrays.asList("Goblin","Troll","Orc","UrukHai"));
        ArrayList<String> darkArmyRiderTypes = new ArrayList<String>(Arrays.asList("Orc"));
        ArrayList<String> darkArmyFeetTypes = new ArrayList<String>(Arrays.asList("Goblin","Troll","UrukHai"));
        
        //армии всадников
        ArrayList <MiddleEarthCitizen> lightArmyRiders = fillArmy(lightArmyRiderTypes);
        ArrayList <MiddleEarthCitizen> darkArmyRiders = fillArmy(darkArmyRiderTypes);
        
        //пешие армии
        ArrayList <MiddleEarthCitizen> lightArmyFeet = fillArmy(lightArmyFeetTypes);
        ArrayList <MiddleEarthCitizen> darkArmyFeet = fillArmy(darkArmyFeetTypes);

        System.out.println();System.out.println("КОННЫЕ АРМИИ:"); PrintList(lightArmyRiders);
        System.out.println(); PrintList(darkArmyRiders);  
        Collections.shuffle(lightArmyRiders);  System.out.println("После шаффла"); PrintList(lightArmyRiders);
        Collections.shuffle(darkArmyRiders); System.out.println("После шаффла"); PrintList(darkArmyRiders);
        Fight(lightArmyRiders, darkArmyRiders);
        
        System.out.println();System.out.println("ПЕШИЕ АРМИИ:"); PrintList(lightArmyFeet);
        System.out.println(); PrintList(darkArmyFeet);
        Collections.shuffle(lightArmyFeet);  System.out.println("После шаффла"); PrintList(lightArmyFeet);
        Collections.shuffle(darkArmyFeet); System.out.println("После шаффла"); PrintList(darkArmyFeet);
        Fight(lightArmyFeet,darkArmyFeet);
        
        Result(lightArmyRiders,lightArmyFeet,darkArmyRiders,darkArmyFeet,2);
        
        
            
    }
    
    public static void Serialize(ArrayList <MiddleEarthCitizen> list) throws FileNotFoundException, IOException
    {
        FileOutputStream file = new FileOutputStream("result.dat");
        ObjectOutputStream obj = new ObjectOutputStream(file);
       // MiddleEarthCitizen citizen = new MiddleEarthCitizen();
        obj.writeObject(list);
        obj.flush();
        obj.close();
        file.close();
    }
    public static void DeSerialize() throws FileNotFoundException, IOException, ClassNotFoundException
    {
        FileInputStream file = new FileInputStream("result.dat");
        ObjectInputStream obj = new ObjectInputStream(file);
        ArrayList <MiddleEarthCitizen> loaded = new ArrayList <MiddleEarthCitizen>(); 
        loaded = (ArrayList<MiddleEarthCitizen>)obj.readObject();
        obj.close();
        PrintList(loaded);
    }
    
    public static void CheckFile() throws IOException, FileNotFoundException, ClassNotFoundException
    {
        try
        {
            DeSerialize();
        }
        catch(FileNotFoundException exception)
        {
             System.out.println("Результатов нет!");
        }
      
    }
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        // TODO code application logic here
        
//       FirstPrint();      
//       SecondPrint();   
//       ThirdPrint();

       CheckFile();
       System.out.println(); System.out.println("Новая сессия");
       LastBattle();

    }   
}
