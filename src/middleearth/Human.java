/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearth;

import middleearth.MiddleEarthCitizen;

/**
 *
 * @author MarkNotebook
 */
public class Human extends MiddleEarthCitizen {
    private String homeTown;
    private int age;
    
    public Human(String homeTown, int age, int height, String name,int power, boolean firstStrike)
    {
        super(name, height,power,firstStrike); //вызывает конструктор супер-класса
        this.homeTown = homeTown;
        this.age = age;
    }
    
    public String getHomeTown(){
        return this.homeTown;
    }
    
    public void setHomeTown(String homeTown){
        this.homeTown= homeTown;
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge(int age){
        this.age=age;
    }
    
    @Override
    public String toString()
    {
        return (super.toString()+ ", страна: "+homeTown+ ", возраст: "+age+" лет");
    }
    
}
