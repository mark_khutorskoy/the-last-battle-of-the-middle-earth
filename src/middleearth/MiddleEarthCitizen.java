/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearth;
import java.io.Serializable;

/**
 *
 * @author MarkNotebook
 */
public class MiddleEarthCitizen implements Serializable{
     public byte version = 100;
     public byte count = 0;
  
    private String name;
    private int height;
    private static String origin = "Средиземье";
    private int power;
    private boolean firstStrike;
    
    public MiddleEarthCitizen(){
        
    }
    
    public MiddleEarthCitizen(String name, int height, int power, boolean firstStrike){
        this.name=name;
        this.height=height;
        this.power = power;
        this.firstStrike = firstStrike;
    }
    
    public boolean getFirstStrike()
    {
        return this.firstStrike;
    }
    
    public void setFirstStrike(boolean firstStrike)
    {
        this.firstStrike = firstStrike;
    }
    
    public int getPower(){
        return this.power;
    }
    public void setPower(int power){
        this.power = power;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String name){
        this.name= name;
    }
    
    public int getHeight(){
        return this.height;
    }
    
    public void setHeight(int height){
        this.height= height;
    }
    
    public double heightToMeters()
    {
        return (((double)(height))/100);
    }
    
    public static String getCountryOrigin()
    {
        return origin;
    }
    
    public String toString()
    {
        
        return ("Имя: "+name+", рост: "+height+"см, "+"происхождение: "+origin+", power: "+power);
    }

    public Object cast(Class<MiddleEarthCitizen> aClass) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
