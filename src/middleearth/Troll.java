/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package middleearth;

/**
 *
 * @author MarkNotebook
 */
public class Troll extends MiddleEarthCitizen {
    private String type;
    
    public Troll(String type,int height, String name, int power, boolean firstStrike)
    {
        super(name,height, power, firstStrike);
        this.type = type;
    }
    
    public String getType()
    {
        return this.type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    @Override
    public String toString()
    {
        return(super.toString()+", тип: "+type);
    }
}
